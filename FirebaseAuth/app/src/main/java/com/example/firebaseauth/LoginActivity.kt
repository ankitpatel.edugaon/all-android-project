package com.example.firebaseauth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {
    lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        val ankit = findViewById<TextView>(R.id.register)
        ankit.setOnClickListener() {

            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)

            val editEmail=findViewById<EditText>(R.id.editEmail)
            val editPass=findViewById<EditText>(R.id.editPass)


            val login = findViewById<Button>(R.id.login)
            login.setOnClickListener {
                auth.signInWithEmailAndPassword(editEmail.text.toString(),editPass.text.toString())
                    .addOnSuccessListener {
                        val intent = Intent(this, HomeActivity::class.java)
                        startActivity(intent)
                        finish()

                        Toast.makeText(this, "login successful", Toast.LENGTH_SHORT).show()
                    }
                    }
        }
    }
}