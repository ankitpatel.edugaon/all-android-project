package com.example.firebaseauth

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class SignUpActivity : AppCompatActivity() {
    lateinit var auth: FirebaseAuth

//    var firebaseFirestore= FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        auth = FirebaseAuth.getInstance()

        val name = findViewById<EditText>(R.id.username)
        val email = findViewById<EditText>(R.id.email)
        val password1 = findViewById<EditText>(R.id.password)

        val registerBtn = findViewById<Button>(R.id.alert)

        registerBtn.setOnClickListener {

            auth.createUserWithEmailAndPassword(email.text.toString(), password1.text.toString()).addOnCompleteListener {

                    Toast.makeText(this, "registerd", Toast.LENGTH_SHORT).show()
//                    val userName = name.text.toString().trim()
//                    val userEmail = email.text.toString().trim()
//                    val userPassword = password1.text.toString().trim()

//                    val hashMap = hashMapOf(
//                        "username" to userName,
//                        "email" to userEmail,
//                        "password" to userPassword
//                    )
//

//                    firebaseFirestore.collection("ankit").add(hashMap)
//                        .addOnSuccessListener {
//                            val sharedPreferences = getSharedPreferences("Register", Context.MODE_PRIVATE).edit()
//                            var docid = it.id
//                            sharedPreferences.putString("userId", docid).apply()
//                            Toast.makeText(this, "Register Successful", Toast.LENGTH_SHORT).show()


                    val intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)
                    finish()


                }
                .addOnFailureListener {
                    Toast.makeText(this, "failed", Toast.LENGTH_SHORT).show()
                }
//        }


//        val ankit=findViewById<TextView>(R.id.Login2)
//        ankit.setOnClickListener{
//            val intent=Intent(this,LoginActivity::class.java)
//            startActivity(intent)
//            finish()
//        }
//
//
//
        }
    }}