package com.example.shareprefapp

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

                // declare share pref
                val sharePreferences = getSharedPreferences("user", Context.MODE_PRIVATE)
                // make editable share preference
                val  editPreferences = sharePreferences.edit()
                // get saved data from share pref
                val  savedName = sharePreferences.getString("name", "")
                val  savedEmail = sharePreferences.getString("email", "")

                // get location of view ( TextView, EditText and Button ) from xml
                val nameEditText = findViewById<EditText>(R.id.name_ediText)
                val emailEditText = findViewById<EditText>(R.id.email_editText)
                val showDetailsTextView = findViewById<TextView>(R.id.showDetails_TextView)
                val submitButton = findViewById<AppCompatButton>(R.id.submit_button)

                // set shaved text in text view
                showDetailsTextView.text = "$savedName $savedEmail"

                submitButton.setOnClickListener {

                    val details = nameEditText.text.toString() + ", " + emailEditText.text.toString()
                    // set text from editText in text showDetailsTextView
                    showDetailsTextView.text = details

                    // add name and email into sharePref
                    editPreferences.putString("name", nameEditText.text.toString())
                    editPreferences.putString("email", emailEditText.text.toString())

                    // save all added data in share pref
                    editPreferences.apply()
                }
            }
        }