package com.example.googlelogin

import com.google.firebase.FirebaseException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider

object callbacks : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
    override fun onCodeSent(verificationId: String, p1: PhoneAuthProvider.ForceResendingToken) {
        GlobalVeriable.id=verificationId
    }
    override fun onVerificationCompleted(p0: PhoneAuthCredential) {
    }

    override fun onVerificationFailed(p0: FirebaseException) {
    }

}
