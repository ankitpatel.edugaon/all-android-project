package com.example.googlelogin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.widget.AppCompatButton
import com.google.firebase.auth.FirebaseAuth

class Profile : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        val logout=findViewById<AppCompatButton>(R.id.Logout)
        logout.setOnClickListener{
            FirebaseAuth.getInstance().signOut()

            val intent = Intent(this, PhoneAuth::class.java)
            startActivity(intent)
            finish()
    }
}
}