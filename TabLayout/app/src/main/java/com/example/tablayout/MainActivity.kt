package com.example.tablayout
import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import example.javatpoint.com.kotlincustomlistview.MyListAdapter

class MainActivity : AppCompatActivity() {

    private lateinit var pager: ViewPager // creating object of ViewPager
    private lateinit var tab: TabLayout // creating object of TabLayout
    private lateinit var bar: Toolbar // creating object of ToolBar

    @SuppressLint("SuspiciousIndentation")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // set the references of the declared objects above
        pager = findViewById(R.id.viewPager)
        tab = findViewById(R.id.tabs)

        // To make our toolbar show the application
        // we need to give it to the ActionBar

        // Initializing the ViewPagerAdapter
        val adapter = ViewPagerAdapter(supportFragmentManager)

        // add fragment to the list
        adapter.addFragment(GeeksFragment(), "Chat")
        adapter.addFragment(CodeFragment(), "Status")
        adapter.addFragment(LeetFragment(), "Call")

        // Adding the Adapter to the ViewPager
        pager.adapter = adapter

        // bind the viewPager with the TabLayout.
        tab.setupWithViewPager(pager)


            val language = arrayOf<String>("Rebel Haidar","Suraj Edugaon","Amit Edugaon","Abhishek","Sonu ji","Sudish Sir","Arman Bhai","Nitesh patel","Wajid Edugaon","Saahil","Sohail","Guru ji","Anu")
            val description = arrayOf<String>(
                "kya ho raha hai bhai",
                "Kotlin ka aaj class hua hai",
                "aaj kya padhai hua ankit",
                "2 din class nhi aayenge to tum kotlin kisi se padh lena",
                "ankit ji kotlin ka hw bhejiye",
                "fragment ka project kaisa chal raha hai",
                "kal mera exam hai isliye mai insutitue mai nahi aaunga",
                "Mai abhi 2 ya 3 din class nhi aa paunga",
                "mera bag achha nhi hai",
                "PHP ka project code bhejo",
                "khana ho gya bhai",
                "Thank you ankit for wishing",
                "ka hota ji"

            )

            val imageId = arrayOf<Int>(
                R.drawable.haidar,R.drawable.suraj,R.drawable.amit,
                R.drawable.abhi,R.drawable.sonuji,R.drawable.sudish,
                R.drawable.arman,R.drawable.img,R.drawable.wajid,
                R.drawable.img_1,R.drawable.sohail,R.drawable.guruji,
                R.drawable.anu
            )

        val myListAdapter = MyListAdapter(this,language,description,imageId)
        val listView = findViewById<ListView>(R.id.listView)
        listView.adapter = myListAdapter


                listView.setOnItemClickListener(){adapterView, view, position, id ->
                    val itemAtPos = adapterView.getItemAtPosition(position)
                    val itemIdAtPos = adapterView.getItemIdAtPosition(position)
                    Toast.makeText(this, "Click on item at $itemAtPos its item id $itemIdAtPos", Toast.LENGTH_LONG).show()
                }
            }


}
