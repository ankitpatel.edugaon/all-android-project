package com.example.apicall

data class UserDataModel(
    val Id:String?=null,
    val Name:String? = null,
    val Email:String? = null,
    val Number:String? = null,
    val imageURL:String? = null
)
