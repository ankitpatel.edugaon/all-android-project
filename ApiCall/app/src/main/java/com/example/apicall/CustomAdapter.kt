package com.example.tablayout

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.apicall.R
import com.example.apicall.UserDataModel

import de.hdodenhof.circleimageview.CircleImageView

class CustomAdaptor(val  dataList: List<UserDataModel>, val context: Context) :BaseAdapter(){


    override fun getCount(): Int {
        // returning size of list. it mean how many item will be
        return  dataList.size
    }

    override fun getItem(position: Int): Any {
        // it will get data according to position
        // Like at 0 index our data is
        // customDataList.add(CustomListModel(name = "Sk", imageURL = "", email = "Sk@gmail.com"))
        return dataList[position]
    }

    override fun getItemId(position: Int): Long {
        // it will convert int position to Long because it's return type data is Long
        return position.toLong()
    }

    @SuppressLint("MissingInflatedId", "ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        // calling / adding (custom_layout_item.xml ) layout of using LayoutInflater
        val rowView = LayoutInflater.from(context).inflate(R.layout.custom_layout_item, parent, false)

        // finding views from rowView variable in which we called our custom_layout_item.xml layout
        val  name = rowView.findViewById<TextView>(R.id.name)
        val  email = rowView.findViewById<TextView>(R.id.email)
        val  number = rowView.findViewById<TextView>(R.id.number)
        val  Id = rowView.findViewById<TextView>(R.id.Id)
        val imageView=rowView.findViewById<CircleImageView>(R.id.image)

        // setting text on TextView like this ( name.text = dataList[position].name )
        // we have out textView in name and email
        Id.text = dataList[position].Id
        name.text = dataList[position].Name
        email.text = dataList[position].Email
        number.text = dataList[position].Number
        val url= dataList[position].imageURL

        Glide.with(context)
            .load(url)
            .error(R.drawable.whatsapp)
            .into(imageView)

        // returning view with data ( custom_layout_item layout and data)
        // Each data will take one view
        return rowView
    }

}