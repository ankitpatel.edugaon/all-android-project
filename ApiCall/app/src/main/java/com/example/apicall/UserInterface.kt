package com.example.apicall

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

    interface UserApiInterface {
        @GET("userdata")
        fun getUser() : Observable<List<UserDataModel>> // object in list
//    fun getUser() : Observable<UserDataModel> // for object

        companion object Factory{
            fun  create() : UserApiInterface{
                val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("https://demo5896499.mockable.io/")
                    .build()

                return  retrofit.create(UserApiInterface::class.java)
            }
        }

    }