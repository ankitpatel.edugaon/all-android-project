package com.example.whatsapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageButton
import androidx.viewpager.widget.ViewPager
import com.example.tablayout.TabsAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewPager = findViewById<ViewPager>(R.id.viewPager_account)
        val tabLayout = findViewById<TabLayout>(R.id.tabLayout_account)

        viewPager.adapter = TabsAdapter(supportFragmentManager)

        tabLayout.setupWithViewPager(viewPager)

        val cambutton=findViewById<ImageButton>(R.id.cambutton)
        cambutton.setOnClickListener{
            val intent = Intent("android.media.action.IMAGE_CAPTURE")
            startActivity(intent)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbarmenu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.newGroup->{
                true
            }
            R.id.broadcast->{
                true
            }
            R.id.Linked->{
                true
            }
            R.id.messages->{
                true
            }
            R.id.Payments->{
                true
            }
            R.id.setting->{
                true
            }
            else->super.onOptionsItemSelected(item)
        }
    }




}