package com.example.whatsapp

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ListView
import com.example.datalist
import com.example.tablayout.CustomAdaptor

class Fragment2 : Fragment() {
    val customDataList=datalist.names
    @SuppressLint("MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rowView = inflater.inflate(R.layout.fragment_2, container, false)
                // Find the WebView by its unique ID
                val webView = rowView.findViewById<WebView>(R.id.web)

                // loading http://www.google.com url in the WebView.
                webView.loadUrl("https://www.youtube.com/")

                // this will enable the javascript.
                webView.settings.javaScriptEnabled = true

                // WebViewClient allows you to handle
                // onPageFinished and override Url loading.
                webView.webViewClient = WebViewClient()
               return rowView
            }
        }


