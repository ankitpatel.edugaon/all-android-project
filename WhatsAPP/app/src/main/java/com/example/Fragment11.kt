package com.example

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import com.example.tablayout.CustomAdaptor
import com.example.whatsapp.R

class Fragment11 : Fragment() {
     val customDataView = datalist.names

        @SuppressLint("MissingInflatedId")
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rowView = inflater.inflate(R.layout.fragment_11, container, false)
            val costListView = rowView.findViewById<ListView>(R.id.cusList)
            costListView.adapter = activity?.let { CustomAdaptor(customDataView, it) }

            return rowView
        }

    }

