package com.example.imobilepay

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.buttonnavigation.second_fragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class HomeActivity : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val firstFragment = first_fragment()
        val secondFragment = second_fragment()
        val thirdFragment = third_fragment()
        val profileFragment = profileFragment()

        setCurrentFragment(firstFragment)

        val button = findViewById<BottomNavigationView>(R.id.buttom)
        button.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.home -> setCurrentFragment(firstFragment)
                R.id.Notification -> setCurrentFragment(secondFragment)
                R.id.setting -> setCurrentFragment(thirdFragment)
                R.id.profile -> setCurrentFragment(profileFragment)
            }
            true
        }
    }
    private fun setCurrentFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.container, fragment)
            transaction.commit()
       }}
