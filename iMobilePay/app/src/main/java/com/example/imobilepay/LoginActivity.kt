package com.example.imobilepay

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider

class LoginActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    val requestCode:Int=123

    var firebaseAuth = FirebaseAuth.getInstance()

    @SuppressLint("MissingInflatedId", "WrongViewCast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth=FirebaseAuth.getInstance()

        // Configure Google Sign In inside onCreate method
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        // getting the value of gso inside the GoogleSignInClient
        val googleSignInClient = GoogleSignIn.getClient(this,gso)
        val googleAuth = findViewById<ImageView>(R.id.googleAuth)
        googleAuth.setOnClickListener{
            val googleIntent = googleSignInClient.signInIntent
            startActivityForResult(googleIntent,requestCode)
        }


        firebaseAuth= FirebaseAuth.getInstance()

        val forget = findViewById<TextView>(R.id.forgetPass)
        forget.setOnClickListener {
            val intent = Intent(this, ResetActivity::class.java)
            startActivity(intent)
            finish()
        }

        val ankit = findViewById<TextView>(R.id.register)
        ankit.setOnClickListener() {
            val intent = Intent(this, RegistrationActivity::class.java)
            startActivity(intent)
            finish()
        }

        val phoneVerify=findViewById<ImageView>(R.id.phoneVerify)
        phoneVerify.setOnClickListener{
            val intent = Intent(this, OtpAuthActivity::class.java)
            startActivity(intent)
            finish()
        }
//        val sharePreferences = getSharedPreferences("Register", Context.MODE_PRIVATE)
//
//        val savedName = sharePreferences.getString("name", "")
//        val savedPassword = sharePreferences.getString("password", "")

        val editEmail = findViewById<EditText>(R.id.editEmail)
        val editPass = findViewById<EditText>(R.id.editPass)


        val login = findViewById<ImageView>(R.id.login)
        login.setOnClickListener {
            if (editEmail.text.isNotEmpty() && editPass.text.isNotEmpty()) {
                auth.signInWithEmailAndPassword(editEmail.text.toString(), editPass.text.toString())
                    .addOnSuccessListener {
//                    val userId = it.user?.uid

                        val intent = Intent(this, HomeActivity::class.java)
                        startActivity(intent)
                        finish()

                        Toast.makeText(this, "login successful", Toast.LENGTH_SHORT).show()
                    }
                    .addOnFailureListener {
                        Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                    }

            }

            else {
                Toast.makeText(this, "please fill email password", Toast.LENGTH_SHORT).show()
              }
        }

    }

    override fun onActivityResult(activityRequestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(activityRequestCode, resultCode, data)

        if (activityRequestCode == requestCode) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            task.addOnSuccessListener { it ->
                val credencial = GoogleAuthProvider.getCredential(it.idToken, null)
                auth.signInWithCredential(credencial)
                    .addOnSuccessListener {
                        startActivity(Intent(this, HomeActivity::class.java))
                        Toast.makeText(this, "" + it.user?.displayName,Toast.LENGTH_SHORT).show()
                    }
                    .addOnFailureListener {
                        Toast.makeText(this, "" + it.message, Toast.LENGTH_SHORT).show()
                    }
            }
                .addOnFailureListener {
                    Toast.makeText(this, "" + it.message, Toast.LENGTH_SHORT).show()

                }
        }
    }

}
//            if(lEmail.text.isNotEmpty()  && pass.text.isNotEmpty()) {
//
//                if (luserName.text.toString() == savedName.toString() &&  pass.text.toString()==savedPassword.toString()) {
//
//                    val loginSharePreferences= getSharedPreferences("Login", Context.MODE_PRIVATE)
//                    loginSharePreferences.edit().putBoolean("LoginKey",true).apply()
//

//
//
//
//                } else {
//                    Toast.makeText(this, "please correct email password", Toast.LENGTH_SHORT).show()
//                }
//            }

//            else{
//                Toast.makeText(this, "please fill all data", Toast.LENGTH_SHORT).show()
//            }
