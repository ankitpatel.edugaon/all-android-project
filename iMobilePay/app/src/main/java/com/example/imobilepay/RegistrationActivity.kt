package com.example.imobilepay

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class RegistrationActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
//
//    val requestCode:Int=123

    var firebaseFirestore= FirebaseFirestore.getInstance()

    @SuppressLint("MissingInflatedId", "CutPasteId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        auth=FirebaseAuth.getInstance()

//        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//            .requestIdToken(getString(R.string.default_web_client_id))
//            .requestEmail()
//            .build()
//
//        // getting the value of gso inside the GoogleSignInClient
//        val googleSignInClient = GoogleSignIn.getClient(this,gso)
//        val googleAuth = findViewById<ImageView>(R.id.googleAuth)
//        googleAuth.setOnClickListener{
//            val googleIntent = googleSignInClient.signInIntent
//            startActivityForResult(googleIntent,requestCode)
//        }

        val username=findViewById<EditText>(R.id.username)
        val userEmail=findViewById<EditText>(R.id.email)
        val userPassword =findViewById<EditText>(R.id.password)

        val registerBtn=findViewById<ImageView>(R.id.alert)

        registerBtn.setOnClickListener {
            if (username.text.isNotEmpty() && userEmail.text.isNotEmpty() && userPassword.text.isNotEmpty()) {
                auth.createUserWithEmailAndPassword(
                    userEmail.text.toString(),
                    userPassword.text.toString()
                )
                    .addOnSuccessListener {

                        val name = username.text.toString().trim()
                        val email = userEmail.text.toString().trim()
                        val password = userPassword.text.toString().trim()

                        val hashMap = hashMapOf<String, Any>(
                            "name" to name,
                            "email" to email,
                            "password" to password
                        )

                        firebaseFirestore.collection("users").add(hashMap)
                            .addOnSuccessListener {

                                val sharedPreferences =
                                    getSharedPreferences("register", Context.MODE_PRIVATE).edit()
                                val docid = it.id
                                sharedPreferences.putString("UserId", docid).apply()

                                val intent = Intent(this, HomeActivity::class.java)
                                startActivity(intent)
                                finish()

                                Toast.makeText(this, "Registration Successful", Toast.LENGTH_SHORT)
                                    .show()
                            }
                    }
                    .addOnFailureListener {
                        Toast.makeText(this, "failed", Toast.LENGTH_SHORT).show()
                    }
            } else {
                Toast.makeText(this, "please fill all data", Toast.LENGTH_SHORT).show()
            }

            val anu =findViewById<TextView>(R.id.Login2)
            anu.setOnClickListener{
                val intent=Intent(this,LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        }

//                val sharePreferences= getSharedPreferences("Register", Context.MODE_PRIVATE)
//                val loginSharePreferences= getSharedPreferences("Login", Context.MODE_PRIVATE)
//                val editPreference= sharePreferences.edit()

//

            }
    }

