package com.example.basicfirebase

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide

import com.example.imobilepay.R

class CustomListAdapter (val dataList:ArrayList<CustomListModel>,val context:Context):BaseAdapter(){
    override fun getCount(): Int {
       return dataList.size
    }

    override fun getItem(position: Int): Any {
        return dataList[position]
    }

    override fun getItemId(position: Int): Long {
        return dataList.size.toLong()
    }

    @SuppressLint("CutPasteId", "ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
       val rootView= LayoutInflater.from(context).inflate(R.layout.custom_list_item,parent,false)

        val data = dataList[position]
        val titleText = rootView.findViewById<TextView>(R.id.title_text)
        val descriptionText = rootView.findViewById<TextView>(R.id.description_text)
        val imageView = rootView.findViewById<ImageView>(R.id.image_icon)

        titleText.text=data.title
        descriptionText.text=data.description
        Glide.with(context).load(data.imageUrl)
            .error(R.drawable.dsc).into(imageView)

        return rootView
    }

}
